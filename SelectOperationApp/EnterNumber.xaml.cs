﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SelectOperationApp
{
    /// <summary>
    /// Логика взаимодействия для EnterNumber.xaml
    /// </summary>
    public partial class EnterNumber : Window
    {
        public EnterNumber()
        {
            InitializeComponent();
            _num = "";
            this.numberTextBox.PreviewTextInput += new TextCompositionEventHandler(NumberTextBoxPreviewTextInput);
        }

        private void FactorialButtonClick(object sender, RoutedEventArgs e)
        {

            string writePath = @"D:\factorial.txt";

            try
            {
                using (StreamWriter stream = new StreamWriter(writePath, false, Encoding.Default))
                {
                    stream.WriteLine(numberTextBox.Text);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Не удалось передать число!");
            }

            AppDomain factorialDomain = AppDomain.CreateDomain("Factorial Domain");
            string assemblyPath = factorialDomain.BaseDirectory + "FactorialApp.exe";
            factorialDomain.ExecuteAssembly(assemblyPath);

        }

        private string _num;
        private void NumberTextBoxPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int number = 0;
            
            if (!Char.IsDigit(e.Text, 0)) e.Handled = true;
            _num += e.Text;
           int.TryParse(_num, out number);

            if (number<10 && number<=0) e.Handled = true;
        }
    }
}
