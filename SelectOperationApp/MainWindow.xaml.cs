﻿using System;
using System.Windows;

namespace SelectOperationApp
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DownloadFileButtonClick(object sender, RoutedEventArgs e)
        {
            AppDomain downloadDomain = AppDomain.CreateDomain("Download Domain");
            string assemblyPath = downloadDomain.BaseDirectory + "DownloadFileApp.exe";
            downloadDomain.ExecuteAssembly(assemblyPath);
        }

        private void PerformCalculationsButtonClick(object sender, RoutedEventArgs e)
        {
            EnterNumber enterNumber = new EnterNumber();
            enterNumber.Show();
        }
    }
}
